/* Exercise 1.13: Rewrite the exercises from § 1.4.1 (p. 13) using for loops. */

//  Exercise 1.13A: Write a program that uses a for to sum the numbers from
//	50 to 100.

#include <iostream>

int main()
{
	int sum = 0;

	for(int number = 50; number <= 100; ++number)
		sum += number;

	std::cout << "The result is " << sum << std::endl;

	return 0;
}

// =============================================================================
/*
	Exercise 1.13B: In addition to the ++ operator that adds 1 to its operand,
	there is a decrement operator (--) that subtracts 1. Use the decrement
	operator to write a for that prints the numbers from ten down to zero.


#include <iostream>

int main()
{

	for (int value = 10; value >= 0; --value)
	{
		std::cout << value << std::endl;
	}

	return 0;
}

*/

// =============================================================================

/*
	Exercise 1.13: Write a program that prompts the user for two integers.
	Print each number in the range specified by those two integers.


#include <iostream>

int main()
{
	int value1 = 0, value2 = 0;

	std::cout << "Enter lower limit: ";
	std::cin >> value1;
	std::cout << "Enter upper limit: ";
	std::cin >> value2;

	std::cout << "The range of values are now displayed...." << std::endl;


	// The initializer is not typed because it was asked at prompt
	for (; value1 <= value2; ++value1)
	{
		std::cout << value1 << " ";
	}


	std::cout << std::endl;

	return 0;
}

*/
