/**
* Exercise 1.16: Write your own version of a program that prints the sum of
* a set of integers read from cin.
*/

#include <iostream>

int main()
{
  int total_sum = 0;
  int values = 0;

  // read the values the user inputs
  std::cout << "Please enter a series of numbers. Type X to stop input:\n";

  while (std::cin >> values) {
    total_sum = total_sum + values;
  }

  // print result
  std::cout << "Total sum of values is " << total_sum << std::endl;

  return 0;
}

