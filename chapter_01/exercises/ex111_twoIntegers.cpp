#include <iostream>

int main()
{
  int number_0 = 0;
  int number_1 = 0;

  std::cout << "Enter two integers:\n";
  std::cin >> number_0 >> number_1;

  for (int i = number_0; i <= number_1; i++) {
    std::cout << i << " ";
  }

  std::cout << "\n\n";

  return 0;
}
