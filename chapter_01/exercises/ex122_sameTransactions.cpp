/**
 * Exercise 1.22: Write a program that reads several transactions for the same
 * ISBN. Write the sum of all the transactions that were read.
 * 
 * test: 0-201-78345-X 5 110 22
*/

#include <iostream>
#include "Sales_item.h"

int main()
{
  Sales_item item;
  short n_items;

  std::cout << "Type number of transactions with same ISBN:\n";
  std::cin >> n_items;
  std::cout << "Type ISBN item:\n";
  std::cin >> item;

  std::cout << "Calculating ================================================\n";
  for (int i = 0; i < n_items; i++) {
    item += item;
  }

  std::cout << "Result: " << item << std::endl;

  return 0;
}